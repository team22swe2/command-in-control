const express = require("express");
const mongoose = require("mongoose");
const path = require("path");
const socket_io = require("socket.io");
require("dotenv").config({ path: __dirname + "/.env" });

var app = express();
var io = socket_io();
app.io = io;

const cicRoute = require("./routes/cic");
const accountservicesRoute = require("./routes/accountservices");

app.use(express.json()); //json parser
app.use("/api/cic", cicRoute);
app.use("/api/accountservices", accountservicesRoute);

//routes
app.get("/", (req, res) => {
  res.send("home endpoint");
});

let connections = new Set();
io.on("connection", function(socket) {
  console.log("new socket connection");

  connections.add(socket);
  socket.on("greet", function(data) {
    console.log(data);
    socket.emit("respond", { hello: "greetings client" });
  });

  socket.once("disconnect", function() {
    console.log("disconnected");
    connections.delete(socket);
  });
});
app.locals.io = io;

mongoose.connect(
  process.env.DB_HOST,
  {
    useNewUrlParser: true,
    useUnifiedTopology: true
  },
  err => {
    if (err) {
      console.log(err);
    } else {
      console.log("Connect to MangoDB successful..");
    }
  }
);

module.exports = app;
