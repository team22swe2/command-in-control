import paho.mqtt.client as mqtt
import pymongo, isodate, datetime, threading, json


myclient = pymongo.MongoClient("mongodb://localhost:27017/")
mydb = myclient["prod"]

# The callback for when the client receives a CONNACK response from the server.
def on_connect(client, userdata, flags, rc):
    print("Connected with result code "+str(rc))

# The callback for when a PUBLISH message is received from the server.
def on_message(client, userdata, msg):
    print(msg.topic+" "+str(msg.payload))

client = mqtt.Client()
client.on_connect = on_connect
client.on_message = on_message
client.connect("127.0.0.1", 1883)

#Finds scheduled diagnostics lte now() and publishes them to the topic
def find_diagnostics():
    threading.Timer(1.0, find_diagnostics).start()
    collection = mydb["scheduled_diagnostics"]
    now = datetime.datetime.now()
    diag = collection.find_one({"time_to_run": {"$lte": now}})
    if diag:
        msg = {"gwID": diag.get('gwID'), "test_queue": diag.get('test_queue')}
        client.publish('test_queue', json.dumps(msg))
        to_delete = collection.delete_one({'_id': diag.get('_id')})
        print(to_delete.deleted_count, " documents deleted.") 
find_diagnostics()

# Blocking call that processes network traffic, dispatches callbacks and
# handles reconnecting.
# Other loop*() functions are available that give a threaded interface and a
# manual interface.
client.loop_forever()

