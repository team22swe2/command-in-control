const express = require('express')
const User = require('../models/User')
const Gateway = require('../models/Gateway')
const auth = require('../middleware/auth')

const router = express.Router()

module.exports = router

router.get('/new_gateway', auth, async(req, res) => {
    const email = req.user.email
    const gwID = req.query.gwID
    const new_gateway = new Gateway({
        owner: email,
        gwID: gwID
    });
    try {   
        await new_gateway.save();
        res.status(201).send(JSON.stringify(new_gateway))
    }
    catch (error) {
        res.status(400).send(error)
    }
});

router.get('/owned_gateways', auth, async(req, res) => {
    const email = req.user.email
    try {   
        Gateway.find({owner: email}, (error, gateways) => {
            res.status(200).send(JSON.stringify(gateways))
        })
    }
    catch (error) {
        res.status(400).send(error)
    }
});

router.get('/user', auth, async(req, res) => {
    // View logged in user profile
    res.send(req.user)
  });
  
router.post('/register', async (req, res) => {
// Create a new user
try {
    const user = new User(req.body)
    await user.save()
    const token = await user.generateAuthToken()
    res.status(201).send({ token })
} catch (error) {
    res.status(400).send(error)
}
})
  
router.post('/login', async(req, res) => {
//Login a registered user
try {
    const { email, password } = req.body
    const user = await User.findByCredentials(email, password)
    if (!user) {
        return res.status(401).send({error: 'Login failed! Check authentication credentials'})
    }
    const token = await user.generateAuthToken()
    res.send({ token })
} catch (error) {
    res.status(400).send(error)
}
})

router.post('/deactivatetoken', auth, async (req, res) => {
// Log user out of the application
try {
    req.user.tokens = req.user.tokens.filter((token) => {
        return token.token != req.token
    })
    await req.user.save()
    res.send()
} catch (error) {
    res.status(500).send(error)
}
})
  
router.post('/deactivatetokens', auth, async(req, res) => {
// Log user out of all devices
try {
    req.user.tokens.splice(0, req.user.tokens.length)
    await req.user.save()
    res.send()
} catch (error) {
    res.status(500).send(error)
}
})