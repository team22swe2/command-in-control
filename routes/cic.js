const express = require("express");
const router = express.Router();
const cicSchema = require("../models/cicSchema");
const Diagnostic = require("../models/Diagnostic");
const SensorsData = require("../models/Sensors");
const ScheduledDiagnostic = require("../models/ScheduledDiagnostic");
const mongoose = require("mongoose");
const mqttHandler = require("../mqtt/MqttHandler");

// Saves tests to scheduled_diagnostics collection --> see diagnostic_manager.py for details on how these are used
router.post("/order_tests", async (req, res) => {
  let t;
  if (req.body.time_to_run) {
    t = req.body.time_to_run;
  } else {
    t = new Date();
  }
  try {
    data = {
      gwID: req.body.gwID,
      test_queue: req.body.test_queue,
      time_to_run: t
    };
    const scheduled_diagnostic = new ScheduledDiagnostic(data);
    console.log(scheduled_diagnostic)
    await scheduled_diagnostic.save();
    res
      .status(201)
      .send(
        `${JSON.stringify(
          scheduled_diagnostic
        )} successfully saved the diagnostic`
      );
  } catch (error) {
    res.status(400).send(error);
  }
});

router.post("/sensors", async (req, res) => {
  // Store sensorsData
  try {
    if (req.body.isSpeeding) {
      req.app.locals.io.emit("isSpeeding", "Speeding sensor triggered.");
    }
    const sensorsData = new SensorsData(req.body);
    await sensorsData.save();
    res.status(201).send({ sensorsData });
  } catch (error) {
    res.status(400).send(error);
  }
});

router.get("/diagnostic", (req, res) => {
  //If gwID specified as get param -> use it to find diagnostics of that gateway,
  //otherwise find any diagnostics up to 100
  let id = req.query.gwID;
  if (id) {
    console.log(id);
    let query = Diagnostic.find({ gwID: { $eq: id } })
      .limit(100)
      .sort({ $natural: -1 });
    let queried = query.exec();
    queried.then(data => {
      res.json(data);
    });
  } else {
    let query = Diagnostic.find()
      .limit(100)
      .sort({ $natural: -1 });
    let queried = query.exec();
    queried.then(data => {
      res.json(data);
    });
  }
});

router.post("/diagnostic", async (req, res) => {
  // Store a diagnostic

  const diagnostic = new Diagnostic(req.body);
  try {
    await diagnostic.save();
    res.status(201).send({ diagnostic });
  } catch (error) {
    res.status(400).send(error);
  }
});

router.get("/heartbeat", (req, res) => {
  //Query the entire heartbeat collection and return the most recent one
  let query = cicSchema
    .find()
    .limit(1)
    .sort({ $natural: -1 });
  //Obtain the promise from the query
  let queried = query.exec();
  //Callback on successful delivery of promise
  queried.then(data => {
    res.json(data);
  });
});

//Deprecated soon -> this will be handled through broker
router.post("/heartbeat", (req, res) => {
  const hb = new cicSchema({
    heartbeat: Date.now()
  });

  console.log("Heartbeat received: { hb }");
  //save to mongoDB and then return json in response
  hb.save()
    .then(data => {
      //Temporary field to better allow GW controller to test messaging queue
      tests = {
        tests: ["memory_test", "storage_test", "important_test"]
      };
      res.json(tests);
    })
    .catch(err => {
      res.json({ message: err });
    });
});

module.exports = router;
