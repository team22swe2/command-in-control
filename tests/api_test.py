import requests, names, random, pytest, json

class TestCommandInControl:

    heartbeat_endpoint = "https://team22.softwareengineeringii.com/api/cic/heartbeat"
    register_endpoint = "https://team22.softwareengineeringii.com/api/accountservices/register"
    login_endpoint = "https://team22.softwareengineeringii.com/api/accountservices/login"
    diagnostic_endpoint = "https://team22.softwareengineeringii.com/api/cic/diagnostic"

    def get_heartbeat(self):
        return requests.get(self.heartbeat_endpoint)

    def register(self, payload):
        res = requests.post(self.register_endpoint, json=payload)
        return res

    def test_heartbeat(self):
        assert(self.get_heartbeat().status_code < 400)

    def test_register(self):
        user = self.dummy_user()
        res = self.register(user)
        data = json.loads(res.text)
        token = data.get("token")
        if token:
            print(f'\n\nCreated new user {user.get("name")} with email {user.get("email")}...')
        assert(token and res.status_code < 400)

    def test_login(self):
        user = self.dummy_user()
        reg_response = self.register(user)
        data = json.loads(reg_response.text)
        token = data.get("token")

        if reg_response.status_code < 400:
            login_payload = {'email': user['email'], 'password': user['password']}
            login_response = requests.post(self.login_endpoint, json=login_payload)
            data = json.loads(login_response.text)
            token = data.get("token")
            if token:
                print(f"\n\nLogged in with user: {user.get('name')}...")
            assert token
        
    def dummy_user(self):
        first_name = names.get_first_name()
        last_name = names.get_first_name()
        dummy_name = f"{first_name} {last_name}"
        dummy_email = f"{first_name}{last_name}{str(random.randint(0, 99))}@testmail.com"
        password = f"{dummy_name}{str(random.randint(0,50))}"
        return {'name': dummy_name,'email': dummy_email,'password': password}

    def test_diagnostic(self):
        diagnostic_response = requests.get(self.diagnostic_endpoint)
        data = json.loads(diagnostic_response.text)
        if data:
            print(f"\n\nReceived diagnostic data...")
        assert (data and diagnostic_response.status_code < 400)
        
