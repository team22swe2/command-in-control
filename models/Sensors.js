const mongoose = require('mongoose')

const sensorsSchema = mongoose.Schema({
    speed: {
    type: Number,
    required: false
    },
    speedLimit: {
        type: Number,
        required: false
    },
    isSpeeding: {
        type: Boolean,
        required: false
    },
    phoneUsage: {
        type: Number,
        required: false
    },
    isBrakingFast: {
        type: Boolean,
        required: false
    },
    isAcceleratingFast: {
        type: Boolean,
        required: false
    },
    isTurningFast: {
        type: Boolean,
        required: false
    },
})

module.exports = mongoose.model("SensorsData", sensorsSchema);