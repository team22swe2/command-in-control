const mongoose = require('mongoose')

const scheduledDiagnosticSchema = mongoose.Schema({
    gwID: {
        type: String,
        required: true,
    },
    test_queue: {
        type: [String],
        required: true
    },
    time_to_run: {
        type: Date,
        required: true
    },

})

module.exports = mongoose.model("scheduled_diagnostic", scheduledDiagnosticSchema);