const mongoose = require('mongoose')

const gatewaySchema = mongoose.Schema({
    gwID: {
        type: String,
        required: true,
        unique: true
    },
    owner: {
        type: String,
        required: true
    }
})

module.exports = mongoose.model("gateway", gatewaySchema);