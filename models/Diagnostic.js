const mongoose = require('mongoose')

const diagnosticSchema = mongoose.Schema({
    gwID: {
        type: String,
        required: true
    },
    testType: {
        type: String,
        required: true
    },
    testResult: {
        type: String,
        required: true
    },
    timestampStart: {
        type: String,
        required: true
    },
    timestampEnd: {
        type: String,
        required: true
    },
    elapsedTime: {
        type: String,
        required: true
    },
    errorReport: {
        type: String,
        required: false
    }
})

module.exports = mongoose.model("diagnostic", diagnosticSchema);