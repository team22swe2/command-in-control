const mongoose = require("mongoose");

const cicSchema = mongoose.Schema({
  heartbeat: {
    type: Date,
    required: false
  }
});

module.exports = mongoose.model("cicSchema", cicSchema);
